**O (Objective): What did we learn today? What activities did you do? What scenes have impressed you?**

1. codeReview：service层中仍存在repo的相关逻辑，没考虑周全只写了一种方法的过滤
2. mysql：复习了sql语句，学习了DDL与DML，为下午的JPA做准备。DDL是直接操作建表，建数据库等。DML是通过操作对象操作数据库数据
3. JPA：使用DML的思想将sql语句封装到依赖中，通过一系列的注解以及配置帮助开发人员更高效的开发后端应用，

**R (Reflective): Please use one word to express your feelings about today's class.**

顺畅

**I (Interpretive): What do you think about this? What was the most meaningful aspect of this activity?**

下午学习过程中我深刻感受到DML的便利，与相比于DDL，之前配置sql语句极大拖慢了开发进度，我需要尽快掌握这项技术

**D (Decisional): Where do you most want to apply what you have learned today? What changes will you make?**

JPA相关注解

